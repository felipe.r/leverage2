const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    username: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    securityQuestion: {type: String, required: true},
    securityAnswer: {type: String, required: true},
    balance: {type: Number, required: true},
    assets: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Asset",
        },
    ],
});

const User = mongoose.model("User", userSchema);
module.exports = User;
