const {MongoClient} = require("mongodb");
console.log(process.env.MODE);
let url = "mongodb://localhost:27017";
if (process.env.MODE == "production") {
    url = `mongodb://felipe_r:${process.env.MONGO_PASSWORD}@192.168.171.67:27017/?authMechanism=DEFAULT&authSource=test`;
} else {
    url = "mongodb://localhost:27017";
}
const mongoClient = new MongoClient(url);
module.exports = mongoClient;
