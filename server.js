const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
require("dotenv").config();
const app = express();
const port = process.env.PORT || 3000;
const userDetails = require("./userDetails.json");
const viewRoutes = require("./routes/viewRoutes");
const apiRoutes = require("./routes/apiRoutes");

const aLoggerMiddleware = (req, res, next) => {
    console.log(req.method, req.url);
    next();
};
app.use(aLoggerMiddleware);
app.use(
    bodyParser.urlencoded({extended: false}),
    bodyParser.json({extended: false})
);
app.use(express.static("public"), express.static("dist"));
app.use(viewRoutes);
app.use("/api", apiRoutes);

async function main() {
    await mongoose.connect("mongodb://127.0.0.1:27017/goose");
    app.listen(port, () => {
        console.log(`Example app listening on port ${port}`);
    });
}

main().catch((err) => console.error(err));
