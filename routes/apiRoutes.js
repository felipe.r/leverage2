const {Router, response} = require("express");
const userRouter = Router();
const apiRouter = Router();
const mongoClient = require("../db/connection");
const ObjectId = require("mongodb").ObjectId;
const {User, Asset} = require("../models");

const targetDb = process.env.MODE == "production" ? "nic" : "leverage";
const db = mongoClient.db(targetDb);

userRouter.get("/all", async (req, res) => {
    try {
        const users = db.collection("users");
        const allUsers = await users.find({}).toArray();
        res.json(allUsers);
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

userRouter.get("/first", async (req, res) => {
    try {
        const users = db.collection("users");
        const firstUser = await users.findOne({});
        res.json(firstUser);
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

userRouter.get("/:id", async (req, res) => {
    console.log(req.params);
    try {
        const searchedUser = await User.findById(req.params.id).populate(
            "assets"
        );
        res.json(searchedUser);
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

userRouter.post("/", async (req, res) => {
    console.log(req.body);
    try {
        const newUser = await User({
            ...req.body,
            balance: 6789,
            assets: [],
        });
        console.log("new user: " + newUser);
        const savedUser = await newUser.save();
        console.log("saved user: " + savedUser);
        // res.sendStatus(200);

        res.json({id: savedUser._id});
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

userRouter.put("/:id", async (req, res) => {
    console.log(req.params);
    try {
        const users = db.collection("users");
        const searchedUser = await users.replaceOne(
            {
                _id: new ObjectId(req.params.id),
            },
            req.body
        );
        res.json(searchedUser);
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

userRouter.patch("/:id", async (req, res) => {
    console.log(req.params);
    console.log(req.body);
    try {
        const users = db.collection("users");
        const searchedUser = await users.updateOne(
            {_id: new ObjectId(req.params.id)},
            {$set: {...req.body}}
        );
        res.json(searchedUser);
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

userRouter.delete("/:id", async (req, res) => {
    console.log(req.params);
    try {
        const users = db.collection("users");
        const searchedUser = await users.deleteOne({
            _id: new ObjectId(req.params.id),
        });
        res.json(searchedUser);
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

apiRouter.use("/user", userRouter);

apiRouter.post("/trade", async (req, res) => {
    try {
        const {numberOfShares, userId, assetType, assetSymbol, tradeType} =
            req.body;

        const finnhubToken = process.env.FINNHUB_TOKEN;

        const numberShares = parseInt(numberOfShares);
        if (tradeType !== "buy") return res.sendStatus(500);

        const quoteResponse = await fetch(
            `https://finnhub.io/api/v1/quote?symbol=${assetSymbol}&token=${finnhubToken}`
        );

        const quoteData = await quoteResponse.json();
        const price = quoteData.c;

        const user = await User.findById(userId).populate("assets");
        const {balance, assets} = user;

        const tradeTotal = price * numberOfShares;
        if (tradeTotal > balance) return res.sendStatus(500);

        const foundAsset = assets.find((asset) => asset.symbol == assetSymbol);

        if (foundAsset) {
            foundAsset.shares = foundAsset.shares + numberShares;
            await foundAsset.save();
        } else {
            const newAsset = new Asset({
                symbol: assetSymbol,
                type: assetType,
                shares: numberShares,
            });
            await newAsset.save();
            user.assets.push(newAsset);
        }

        user.balance = user.balance - tradeTotal;
        await user.save();
        res.sendStatus(200);
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

module.exports = apiRouter;
