const MDCTextField = mdc.textField.MDCTextField;
const mdcTextFields = [].map.call(
  document.querySelectorAll(".mdc-text-field"),
  function (el) {
    return new MDCTextField(el);
  }
);

const loginForm = document.forms[0];
const registrationForm = document.forms[1];
const registerButton = registrationForm[6];

function validateEligibility(event) {
  event.preventDefault();
  if (noEmptyFields() && passwordsMatch() && legalCheckboxChecked()) {
    registerUser();
  } else {
    console.log("not so valid");
  }
}
registerButton.addEventListener("click", validateEligibility);

function isNotEmpty(input) {
  return input.value !== "";
}

const usernameInput = registrationForm[0];
const passwordInput = registrationForm[1];
const confirmPasswordInput = registrationForm[2];
const securityQuestionInput = registrationForm[3];
const securityAnswerInput = registrationForm[4];

const registrationFormInputs = [
  usernameInput,
  passwordInput,
  confirmPasswordInput,
  securityQuestionInput,
  securityAnswerInput,
];

function noEmptyFields() {
  return registrationFormInputs.every(isNotEmpty);
}

function passwordsMatch() {
  return passwordInput.value === confirmPasswordInput.value;
}

function legalCheckboxChecked() {
  return registrationForm[5].checked;
}

async function registerUser() {
  try {
    const theData = {
      username: usernameInput.value,
      password: passwordInput.value,
      securityQuestion: securityQuestionInput.value,
      securityAnswer: securityAnswerInput.value,
    };
    console.log(theData);
    const response = await fetch("/api/user", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(theData),
    });
    const data = response.json();

    console.log("Success:", data);
  } catch (error) {
    console.error("Error:", error);
  }
}
